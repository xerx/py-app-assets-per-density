import os
import shutil
from PIL import Image
from resizeimage import resizeimage

class densityProps:
    def __init__(self, factor, name_specific):
        self.factor = factor
        self.name_specific = name_specific

def saveIOSImage(source, name, format):
    for prop in iOS_props:
        file_name = name.replace(format, "") + prop.name_specific + format
        if(prop.factor < 1):
            temp_image = resizeimage.resize_width(source, source.size[0] * prop.factor)
        else:
            temp_image = source_image
        temp_image.save(IOS_EXPORT_DIR + "\\" + file_name, source.format, dpi=[72,72])

def saveAndroidImage(source, name, format):
    for prop in android_props:
        specific_path = ANDROID_EXPORT_DIR + "\\" + prop.name_specific
        if(not(os.path.exists(specific_path))):
            os.mkdir(specific_path)
        if(prop.factor < 1):
            temp_image = resizeimage.resize_width(source, source.size[0] * prop.factor)
        else:
            temp_image = source_image
        temp_image.save(specific_path + "\\" + name, source.format, dpi=[72,72])

def saveUWPImage(source, name, format):
    for prop in uwp_props:
        file_name = name.replace(format, "") + "." + prop.name_specific + format
        if(prop.factor < 1):
            temp_image = resizeimage.resize_width(source, source.size[0] * prop.factor)
        else:
            temp_image = source_image
        temp_image.save(UWP_EXPORT_DIR + "\\" + file_name, source.format, dpi=[72,72])


iOS_props = [densityProps(0.66, "@2x"),
             densityProps(1, "@3x")]
android_props = [densityProps(0.25, "drawable"),
                 densityProps(0.33, "drawable-hdpi"),
                 densityProps(0.50, "drawable-xhdpi"),
                 densityProps(0.75, "drawable-xxhdpi"),
                 densityProps(1, "drawable-xxxhdpi")]
uwp_props = [densityProps(0.250, "scale-100"),
             densityProps(0.312, "scale-125"),
             densityProps(0.375, "scale-150"),
             densityProps(0.500, "scale-200"),
             densityProps(1, "scale-400")]
#get source folder
source_folder_path = input("[+] Source folder path: ")

IOS_EXPORT_DIR = source_folder_path + "\\..\\iOS" 
ANDROID_EXPORT_DIR = source_folder_path + "\\..\\Android"
UWP_EXPORT_DIR = source_folder_path + "\\..\\UWP"

#create folders per platform
if(not(os.path.exists(IOS_EXPORT_DIR))):
    os.mkdir(IOS_EXPORT_DIR)
if(not(os.path.exists(ANDROID_EXPORT_DIR))):
    os.mkdir(ANDROID_EXPORT_DIR)
if(not(os.path.exists(UWP_EXPORT_DIR))):
    os.mkdir(UWP_EXPORT_DIR)

folder_contents = os.listdir(source_folder_path)
for item in folder_contents:
    if(item.endswith(".png") or item.endswith(".jpg")):
        print("[~] Resizing " + item)
        source_image = Image.open(source_folder_path + "\\" + item)
        image_format = ".jpg" if source_image.format == "JPEG" else ".png"
        saveIOSImage(source_image, item, image_format)
        saveAndroidImage(source_image, item, image_format)
        saveUWPImage(source_image, item, image_format)
